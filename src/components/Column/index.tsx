import React, { useState, useEffect } from 'react';
import { css } from 'styled-components';
import { Container } from '../../ui/Container';
import { InputForm } from '../AddCardForm';
import { CardList } from '../CardList';
import { ICard } from '../../interfaces';
import { Title } from '../../ui/Title';
import { Clickable } from '../../ui/Clickable';
import { Input } from '../../ui/Input';

interface ColumnProps {
  colName: string
}

export const Column:
React.FunctionComponent<ColumnProps> = ({ colName }: ColumnProps) => {
  const [cards, setCards] = useState<ICard[]>([]);

  useEffect(() => {
    const saved = JSON.parse(
      localStorage.getItem(colName.toString()) || '[]',
    ) as ICard[];
    setCards(saved);
  }, [colName]);

  useEffect(() => {
    localStorage.setItem(colName.toString(), JSON.stringify(cards));
  }, [cards, colName]);

  const addCardHandler = (cardName: string):void => {
    const newCard: ICard = {
      name: cardName,
      id: Date.now(),
      author: localStorage.getItem('username')!,
    };
    setCards(() => [newCard, ...cards]);
    localStorage.setItem(
      `${newCard.name}_${newCard.id}_desc`, 'This card has no description yet!',
    );
  };

  const removeCardHandler = (id: number, name: string):void => {
    setCards((prev) => prev.filter((card) => card.id !== id));
    localStorage.removeItem(`${name}_${id}_comments`);
    localStorage.removeItem(`${name}_${id}_desc`);
  };

  const [title, setTitle] = useState(colName);
  const [titleEditHide, setTitleEditHide] = useState(true);

  const changeTitle = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setTitle(event.target.value);
  };

  const keyPressTitle = (event: React.KeyboardEvent): void => {
    if (event.key === 'Enter' && title !== '') {
      const newCol = localStorage.getItem(colName)!;
      localStorage.setItem(title, newCol);
      localStorage.removeItem(colName);
      const colsList = localStorage.getItem('colsList')!;
      const newColsList = colsList.replace(colName, title);
      localStorage.setItem('colsList', newColsList);
      setTitleEditHide(true);
    }
  };

  return (
    <Container
      css={css`
        flex-direction: column;
        justify-content: flex-start;
        align-items: flex-start;
        padding: 2rem 1rem 0;
        width: 23%;
        background: #C1FFD6;
      `}
    >
      <Container
        css={css`
          width: 100%;
          flex-wrap: nowrap;
          justify-content: space-between;
          align-items: flex-start;
        `}
      >
        <Title text={title} />
        <Clickable
          onClick={() => { setTitleEditHide(!titleEditHide); }}
        >
          <span className="material-icons-outlined">
            edit
          </span>
        </Clickable>
      </Container>
      <Input
        value={title}
        onChange={changeTitle}
        onKeyPress={keyPressTitle}
        hidden={titleEditHide}
      />
      <CardList
        cards={cards}
        onRemove={removeCardHandler}
        colName={colName}
      />
      <InputForm onAdd={addCardHandler} />
    </Container>
  );
};
