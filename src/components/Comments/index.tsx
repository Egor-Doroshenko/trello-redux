import React, { useState, useEffect } from 'react';
import { css } from 'styled-components';
import { Container } from '../../ui/Container';
import { Subtitle } from '../../ui/Subtitle';
import { Text } from '../../ui/Text';
import { IComment } from '../../interfaces';
import { Button } from '../../ui/Button';
import { Input } from '../../ui/Input';
import { commsNum } from '../../utils/commsNum';
import { Clickable } from '../../ui/Clickable';

interface CommentsProps {
  cardName: string,
  cardId: number,
  setComm: React.Dispatch<React.SetStateAction<number>>,
}

export const Comments: React.FunctionComponent<CommentsProps> = (
  {
    cardName, setComm, cardId,
  } : CommentsProps,
) => {
  const [comments, setComments] = useState<IComment[]>([]);
  const [comment, setComment] = useState('');

  const username = localStorage.getItem('username')!;

  const changeComment = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setComment(event.target.value);
  };

  const keyPressComment = (event: React.KeyboardEvent): void => {
    if (event.key === 'Enter' && comment !== '') {
      setComment(comment);
      const newComment: IComment = {
        author: username,
        text: comment,
        id: Date.now(),
      };
      setComments(() => [newComment, ...comments]);
      setComment('');
      setComm(commsNum(cardName, cardId) + 1);
    }
  };

  const clickComment = (): void => {
    setComment(comment);
    const newComment: IComment = {
      author: username,
      text: comment,
      id: Date.now(),
    };
    setComments(() => [newComment, ...comments]);
    setComment('');
    setComm(commsNum(cardName, cardId) + 1);
  };

  const [notMyCommHide, setNotMyCommHide] = useState(true);

  const deleteComment = (id: number, name: string): void => {
    const currentUser = localStorage.getItem('username');
    if (name === currentUser) {
      const comms = JSON.parse(
      localStorage.getItem(`${cardName}_${cardId}_comments`)!,
      ) as IComment[];
      const newComms = comms.filter(
        (c) => (c.id !== id),
      );
      localStorage.setItem(
        `${cardName}_${cardId}_comments`, JSON.stringify(newComms),
      );
      localStorage.removeItem(`${cardName}_${cardId}_comments`);
      localStorage.removeItem(`${cardName}_${cardId}_desc`);
      setComments(newComms);
      setComm(commsNum(cardName, cardId));
    } else {
      setNotMyCommHide(false);
      setTimeout(() => {
        setNotMyCommHide(true);
      }, 5000);
    }
  };

  const [commHidden, setCommHidden] = useState('');
  const [commEditId, setCommEditId] = useState(1);

  const editComment = (
    id:number, text: string, user: string,
  ): void => {
    const currentUser = localStorage.getItem('username');
    if (currentUser === user) {
      setCommHidden(text);
      setCommEditId(id);
    } else {
      setNotMyCommHide(false);
      setTimeout(() => {
        setNotMyCommHide(true);
      }, 5000);
    }
  };

  const inputHide = (
    id: number,
  ): boolean => {
    if (id === commEditId) {
      return (false);
    }
    return (true);
  };

  const editCommentChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    setCommHidden(event.target.value);
  };

  const saveEditedComment = (
    event: React.KeyboardEvent<HTMLInputElement>,
  ): void => {
    if (event.key === 'Enter' && commHidden !== '') {
      const comms = JSON.parse(localStorage.getItem(
        `${cardName}_${cardId}_comments`,
      )!) as IComment[];
      comms.forEach((c) => {
        if (c.id === commEditId) {
          c.text = commHidden;
        }
      });

      localStorage.setItem(
        `${cardName}_${cardId}_comments`, JSON.stringify(comms),
      );
      setCommHidden('');
      const savedComments = JSON.parse(
        localStorage.getItem(`${cardName}_${cardId}_comments`) || '[]',
      ) as IComment[];
      setComments(savedComments);
      setCommEditId(0);
    }
  };

  useEffect(() => {
    const savedComments = JSON.parse(
      localStorage.getItem(`${cardName}_${cardId}_comments`) || '[]',
    ) as IComment[];
    setComments(savedComments);
  }, [cardName, cardId]);

  useEffect(() => {
    localStorage.setItem(
      `${cardName}_${cardId}_comments`, JSON.stringify(comments),
    );
  }, [comments, cardName, cardId]);

  return (
    <>
      <Container
        css={css`
          padding: 0;
          flex-direction: column;
          align-items: flex-start;
          width: 100%;
        `}
      >
        <Input
          onChange={changeComment}
          onKeyPress={keyPressComment}
          value={comment}
          placeholder="Type comment here"
          css={css`
            width: 100%;
          `}
        />
        <Button
          onClick={clickComment}
          text="Add comment"
        />
      </Container>

      {comments.map((comm) => (
        <Container
          css={css`
            padding: 1rem 2rem;
            flex-direction: column;
            width: 100%;
            align-items: flex-start;
            background: #84FFAD;
          `}
          key={comm.id}
        >
          <Container
            css={css`
              width: 100%;
              justify-content: space-between;
            `}
          >
            <Subtitle text={comm.author} />
            <Text
              text="You can edit and delete your own comments only!"
              bold
              hidden={notMyCommHide}
            />
            <span
              className="material-icons-outlined click"
              onClick={() => { deleteComment(comm.id, comm.author); }}
              onKeyPress={() => { deleteComment(comm.id, comm.author); }}
              role="button"
              tabIndex={0}
            >
              close
            </span>
          </Container>

          <Container
            css={css`
              width: 100%;
              flex-wrap: nowrap;
              justify-content: space-between;
              align-items: flex-start;
            `}
          >
            <Text
              size="16px"
              text={comm.text}
            />
            <Clickable
              onClick={() => { editComment(comm.id, comm.text, comm.author); }}
            >
              <span className="material-icons-outlined">
                edit
              </span>
            </Clickable>
          </Container>
          <Input
            hidden={inputHide(comm.id)}
            onChange={editCommentChange}
            onKeyPress={saveEditedComment}
            value={commHidden}
          />
        </Container>
      ))}

    </>
  );
};
