import { IComment } from '../interfaces';

export const commsNum = (cardName: string, cardId: number): number => {
  const comms = JSON.parse(
  localStorage.getItem(`${cardName}_${cardId}_comments`)!,
  ) as IComment[];
  if (comms) {
    return (
      comms.length
    );
  }
  return (
    0
  );
};
